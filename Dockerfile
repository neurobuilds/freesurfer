ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG FREESURFER_VERSION=v7.4.0

FROM ${BASE_DISTRO}:${BASE_VERSION}
ARG DEBIAN_FRONTEND=noninteractive
ARG BASE_DISTRO
ARG BASE_VERSION
ARG PACKAGE_MANAGER
ARG FREESURFER_VERSION

RUN apt-get update -qq && \
    apt-get install -y -q \
            bc \
            ca-certificates \
            curl \
            libgomp1 \
            libxmu6 \
            libxt6 \
            tcsh \
            perl && \
    rm -rf /var/lib/apt/lists/*

ENV FREESURFER_HOME=/usr/local/freesurfer/${FREESURFER_VERSION}

RUN MAJOR_VERSION=$(echo ${BASE_VERSION} | sed 's/\..*//'); \
    VERSION_NUM=$(echo ${FREESURFER_VERSION} | sed 's/v//'); \
    curl -L -O https://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/${VERSION_NUM}/freesurfer_${BASE_DISTRO}${MAJOR_VERSION}-${VERSION_NUM}_amd64.deb \
    apt install ./freesurfer_${BASE_DISTRO}${MAJOR_VERSION}-${VERSION_NUM}_amd64.deb \
    rm -f ./freesurfer_${BASE_DISTRO}${MAJOR_VERSION}-${VERSION_NUM}_amd64.deb

ENV SUBJECTS_DIR=${FREESURFER_HOME}/subjects
ENV LOCAL_DIR=${FREESURFER_HOME}/local
ENV FSFAST_HOME=${FREESURFER_HOME}/fsfast
ENV FMRI_ANALYSIS_DIR=${FREESURFER_HOME}/fsfast
ENV FUNCTIONALS_DIR=${FREESURFER_HOME}/sessions
ENV FS_OVERRIDE=0
ENV FIX_VERTEX_AREA=
ENV FSF_OUTPUT_FORMAT=nii.gz
ENV MINC_BIN_DIR=${FREESURFER_HOME}/mni/bin
ENV MINC_LIB_DIR=${FREESURFER_HOME}/mni/lib
ENV MNI_DIR=${FREESURFER_HOME}/mni
ENV MNI_DATAPATH=${FREESURFER_HOME}/mni/data
ENV MNI_PERL5LIB=${FREESURFER_HOME}/mni/share/perl5
ENV PERL5LIB=${FREESURFER_HOME}/mni/share/perl
ENV PATH=${FREESURFER_HOME}/bin:${FSFAST_HOME}/bin:${FREESURFER_HOME}/tktools:${MINC_BIN_DIR}:$PATH

ENTRYPOINT '/bin/bash'